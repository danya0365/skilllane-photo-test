//
//  Photo.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 8/12/2563 BE.
//

import RealmSwift
import SwiftyJSON

class Photo: Object {
    @objc dynamic var id = ""
    @objc dynamic var author = ""
    @objc dynamic var width = 0
    @objc dynamic var height = 0
    @objc dynamic var url = ""
    @objc dynamic var downloadUrl = ""
    @objc dynamic var isDownloaded = false
    @objc dynamic var createdAt = Date(timeIntervalSince1970: 1)
    @objc dynamic var orderNumber = 0
    @objc dynamic var localImageName = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(_ dictionary: JSON) {
        self.init()
        id = dictionary["id"].string ?? ""
        author = dictionary["author"].string ?? ""
        width = dictionary["width"].int ?? 0
        height = dictionary["height"].int ?? 0
        url = dictionary["url"].string ?? ""
        downloadUrl = dictionary["download_url"].string ?? ""
        orderNumber = dictionary["order_number"].int ?? 0
    }
}

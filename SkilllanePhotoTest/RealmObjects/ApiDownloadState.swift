//
//  ApiDownloadState.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 8/12/2563 BE.
//

import RealmSwift

class ApiDownloadState: Object {
    @objc dynamic var name = ""
    @objc dynamic var limit = 0
    @objc dynamic var page = 0
    
    override static func primaryKey() -> String? {
        return "name"
    }
}

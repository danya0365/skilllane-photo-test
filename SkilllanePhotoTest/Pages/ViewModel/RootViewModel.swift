//
//  RootViewModel.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 8/12/2563 BE.
//

import RealmSwift
import Alamofire
import SwiftyJSON
import PKHUD

class RootViewModel {
    //MARK: - Properties
    let realm = try! Realm()
    var onPresentPhotoPage: (() -> Void)?
    var onPresentLoginPage: (() -> Void)?
    let photoService = PhotoService.shareInstance
    
    //MARK: - Functions
    func checkPresentingPage() {
        
        if let _ = realm.objects(Token.self).first {
            self.requestDataFromApi()
        } else {
            self.onPresentLoginPage?()
        }
    }
    
    func requestDataFromApi() {
        
        HUD.show(.label("Request Photo..."))
        photoService.requestDataFromApi {
            HUD.hide()
            self.onPresentPhotoPage?()
        } _: { (errorMessage) in
            HUD.hide()
        }
    }
}

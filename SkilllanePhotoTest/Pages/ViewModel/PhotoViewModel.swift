//
//  PhotoViewModel.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 8/12/2563 BE.
//

import RealmSwift
import Alamofire

class PhotoViewModel {
    //MARK: - Properties
    let realm = try! Realm()
    var currentPhoto: Photo?
    var request: Alamofire.Request?
    var isShowNextPhotoAfterDownloaded = false
    var isOnRequestNextPage = false
    let photoService = PhotoService.shareInstance
    let photoFileService = PhotoFileService.shareInstance
    var onStatusUpdate: ((_ statusText: String) -> Void)?
    var onProgressUpdate: ((_ progress: Float) -> Void)?
    var onDownloadCompleted: (() -> Void)?
    
    //MARK: - Functions
    func setUpData() {

        if currentPhoto == nil {
            if let firstPhoto = realm.objects(Photo.self).sorted(byKeyPath: "orderNumber", ascending: true).first {
                currentPhoto = firstPhoto
            }
        }
    }
    
    func nextApiDownload(_ callback: @escaping () -> () ) {
        guard !isOnRequestNextPage else { return }
        isOnRequestNextPage = true
        
        photoService.page += 1
        photoService.requestDataFromApi {
            self.isOnRequestNextPage = false
            callback()
        } _: { (errorMessage) in
            self.isOnRequestNextPage = false
        }

    }
    
    
    func startDownloadPhotos(_ callback: @escaping () -> () ) {
        
        guard let _ = realm.objects(Photo.self).filter("isDownloaded = false").first else {
            self.onStatusUpdate?("Download completed!")
            self.onDownloadCompleted?()
            callback()
            return
        }
        
        let taskGroup = DispatchGroup()
        
        let allPhotos = realm.objects(Photo.self).filter("isDownloaded = false")
        for photo in allPhotos {
            
            taskGroup.enter()
            httpDownloadPhoto(photo) {
                taskGroup.leave()
            }
        }

        taskGroup.notify(queue: .main) {
            callback()
        }
    }
    
    func downloadPhoto(_ callback: @escaping () -> () ) {

        guard let photo = realm.objects(Photo.self).filter("isDownloaded = false").first else {
            self.onStatusUpdate?("Download completed!")
            self.onDownloadCompleted?()
            callback()
            return
        }
   
        httpDownloadPhoto(photo) {
            self.downloadPhoto {
                callback()
            }
        }
    }
    
    private func httpDownloadPhoto(_ photo: Photo, _ callback: @escaping () -> () ) {
        let url = photo.downloadUrl

        DispatchQueue.global(qos: .background).async {
            
            self.request = AF.request(url).downloadProgress { [unowned self] (progress) in
                
                let completedSize = Int(progress.completedUnitCount) / 1000
                let totalSize = Int(progress.totalUnitCount) / 1000
                self.onStatusUpdate?("Download Author \(photo.author) => \(completedSize)/\(totalSize)KB")
                self.onProgressUpdate?( Float(progress.fractionCompleted) )
                
            }.responseData { [unowned self] (response) in
                
                switch (response.result) {

                     case .success( _):
                        
                        if let imageData = response.value {
                            let localImageName = "\(photo.orderNumber).png"
                            realm.beginWrite()
                            photo.isDownloaded = true
                            photo.localImageName = localImageName
                            try! realm.commitWrite()
                            
                            self.photoFileService.saveImage(localImageName, image: UIImage(data: imageData)!)
                        }
                        
                        break
                     case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                        break
                 }
                 callback()
            }
        }
        

    }
    
    func redownloadPhoto(_ callback: @escaping () -> () ) {
        guard let _currentPhoto = currentPhoto else {
            return
        }
        realm.beginWrite()
        _currentPhoto.isDownloaded = false
        try! realm.commitWrite()
        
        self.downloadPhoto {
            callback()
        }
    }
    
    func getNextPhoto(_ callback: @escaping () -> () ) {
        isShowNextPhotoAfterDownloaded = false
        guard let _currentPhoto = currentPhoto else {
            return
        }
        
        if let photo = realm.objects(Photo.self).filter("  orderNumber > \(_currentPhoto.orderNumber) AND isDownloaded = true").sorted(byKeyPath: "orderNumber", ascending: true).first {
            currentPhoto = photo
            callback()
        } else {
            isShowNextPhotoAfterDownloaded = true
            nextApiDownload{
                self.downloadPhoto {
                    if self.isShowNextPhotoAfterDownloaded {
                        self.getNextPhoto{
                            callback()
                        }
                    }
                }
            }
        }
    }
    
    func getPrevPhoto(_ callback: @escaping () -> () ) {
        isShowNextPhotoAfterDownloaded = false
        guard let _currentPhoto = currentPhoto else {
            return
        }

        if let photo = realm.objects(Photo.self).filter(" orderNumber < \(_currentPhoto.orderNumber) AND isDownloaded = true ").sorted(byKeyPath: "orderNumber", ascending: false).first {
            
            currentPhoto = photo
            callback()
        } else {
            
        }
    }
    
    func signOut(_ onSuccess: @escaping () -> (), onFailure: @escaping (_ error: String?) -> () )  {
        
        let loginService = LoginService.shareInstance
        loginService.signOut {
            onSuccess()
        } onFailure: { (errorMessage) in
            onFailure(errorMessage)
        }
    }
}

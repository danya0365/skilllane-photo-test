//
//  LoginViewModel.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 8/12/2563 BE.
//

import RealmSwift

class LoginViewModel {
    //MARK: - Properties
    let loginService = LoginService.shareInstance
    
    //MARK: - Functions
    func signIn(_ userName: String, _ password: String, onSuccess: @escaping () -> (), onFailure: @escaping (_ error: String?) -> () )  {
        
        loginService.signIn {
            onSuccess()
        } onFailure: { (errorMessage) in
            onFailure(errorMessage)
        }
    }
}

//
//  RootViewController.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 8/12/2563 BE.
//

import UIKit
import RealmSwift
import PKHUD

class RootViewController: BaseViewController {

    //MARK: - Properties
    var viewModel = RootViewModel()
    
    //MARK: - Functions
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.onPresentPhotoPage = { [unowned self] in
            self.presentPhoto()
        }
        viewModel.onPresentLoginPage = { [unowned self] in
            self.presentLogin()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.checkPresentingPage()
    }
    
    //MARK: Controller
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func presentLogin() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        guard let loginNavigation:BaseNavigationViewController = storyboard.instantiateViewController(withIdentifier: "LoginNavigation") as? BaseNavigationViewController else { return }
        loginNavigation.modalPresentationStyle = .fullScreen
        let loginViewController:LoginViewController? = loginNavigation.visibleViewController as? LoginViewController
        loginViewController?.delegate = self
        self.present(loginNavigation, animated: false)
    }
    
    func presentPhoto() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        guard let photoViewNavigation:BaseNavigationViewController = storyboard.instantiateViewController(withIdentifier: "PhotoViewNavigation") as? BaseNavigationViewController else { return }
        
        photoViewNavigation.modalPresentationStyle = .fullScreen
        let photoViewController:PhotoViewController? = photoViewNavigation.visibleViewController as? PhotoViewController
        photoViewController?.delegate = self
        self.present(photoViewNavigation, animated: false)
    }
}

extension RootViewController: LoginViewControllerDelegate
{
    func loginViewControllerOnSuccess(_ viewController: LoginViewController) {
        viewController.dismiss(animated: false) {

        }
    }
}

extension RootViewController: PhotoViewControllerDelegate
{
    func photoViewControllerOnSignOut(_ viewController: PhotoViewController) {
        viewController.dismiss(animated: false) {

        }
    }
}

//
//  PhotoViewController.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 8/12/2563 BE.
//

import UIKit
import RealmSwift
import PKHUD

protocol PhotoViewControllerDelegate {
    func photoViewControllerOnSignOut(_ viewController: PhotoViewController)
}

class PhotoViewController: BaseViewController {

    var delegate:PhotoViewControllerDelegate?
    var viewModel = PhotoViewModel()
    
    @IBOutlet weak var authorValueLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var downloadProgressView: UIProgressView!
    @IBOutlet weak var statusValueLabel: UILabel!
    
    //MARK: - Functions
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.setUpData()
        viewModel.downloadPhoto {
            self.assignDataToView()
        }
        viewModel.onStatusUpdate = { message in
            self.statusValueLabel.text = message
            
            if self.statusValueLabel.isHidden {
                self.statusValueLabel.isHidden = false
                self.statusValueLabel.alpha = 0
                UIView.animate(withDuration: 0.3) {
                    self.statusValueLabel.alpha = 1
                } completion: { (_) in
                    
                }
            }
        }
        viewModel.onProgressUpdate = { progress in
            self.downloadProgressView.progress = progress
            
            if self.downloadProgressView.isHidden {
                self.downloadProgressView.isHidden = false
                self.downloadProgressView.alpha = 0
                UIView.animate(withDuration: 0.3) {
                    self.downloadProgressView.alpha = 1
                } completion: { (_) in
                    
                }
            }
        }
        viewModel.onDownloadCompleted = {
            self.downloadProgressView.isHidden = false
            self.downloadProgressView.alpha = 1
            self.statusValueLabel.isHidden = false
            self.statusValueLabel.alpha = 1
            UIView.animate(withDuration: 0.3) {
                self.downloadProgressView.alpha = 0
                self.statusValueLabel.alpha = 0
            } completion: { (_) in
                self.downloadProgressView.isHidden = true
                self.statusValueLabel.isHidden = true
            }
            self.assignDataToView()
        }
        setUpRightBarButtonItem()
        setUpObserverApplicationNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: Controller
    func setUpObserverApplicationNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive(notification:)), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground(notification:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    @objc func applicationDidBecomeActive(notification: NSNotification) {
        viewModel.request?.resume()
    }
    
    @objc func applicationDidEnterBackground(notification: NSNotification) {
        viewModel.request?.suspend()
    }
    
    func assignDataToView() {
        guard let photo = viewModel.currentPhoto else { return }
        let isImageDownloaded = viewModel.photoFileService.isImageDownloaded(photo.localImageName)
        if !isImageDownloaded {
            viewModel.redownloadPhoto {
                self.assignDataToView()
            }
            return
        }
        authorValueLabel.text = photo.author
        photoImageView.image = UIImage(contentsOfFile: viewModel.photoFileService.getImagePath(photo.localImageName))
    }
    
    func setUpRightBarButtonItem() {
        let signOutButton = UIButton.init(type: .custom)
        signOutButton.setTitle("Sign Out", for: .normal)
        signOutButton.setTitleColor(.black, for: .normal)
        signOutButton.addTarget(self, action:#selector(signOutButtonClicked), for: .touchUpInside)
        signOutButton.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: signOutButton)
        navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func signOutButtonClicked() {
        
        HUD.show(.label("Sign Out..."))
        viewModel.signOut {
            
            HUD.hide()
            self.delegate?.photoViewControllerOnSignOut(self)
            
        } onFailure: { (errorMessage) in
            HUD.hide()
        }
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let location = touches.first?.location(in: self.view) else { return }

        let touchAreaWidth: CGFloat = self.view.frame.size.width/2

        if location.x <= touchAreaWidth {
            showPrevPhoto()
        } else if location.x >= (self.view.frame.size.width - touchAreaWidth) {
            showNextPhoto()
        } else {
            //
        }
    }
    
    func showNextPhoto() {
        viewModel.getNextPhoto {
            self.assignDataToView()
        }
    }
    
    func showPrevPhoto() {
        viewModel.getPrevPhoto {
            self.assignDataToView()
        }
    }
}

//
//  LoginViewController.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 8/12/2563 BE.
//

import UIKit
import RealmSwift
import PKHUD

protocol LoginViewControllerDelegate {
    func loginViewControllerOnSuccess(_ viewController: LoginViewController)
}

class LoginViewController: BaseViewController {

    //MARK: - Properties
    var delegate:LoginViewControllerDelegate?
    var viewModel = LoginViewModel()
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //MARK: - Functions
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: Controller
    @IBAction func loginClick(_ sender: Any) {
        
        guard let userName = userNameTextField.text, !userName.isEmpty else {
            return
        }
        guard let password = passwordTextField.text, !password.isEmpty else {
            return
        }
        
        HUD.show(.label("Sign In..."))
        viewModel.signIn(userName, password) {
            
            HUD.hide()
            self.delegate?.loginViewControllerOnSuccess(self)
            
        } onFailure: { (errorMessage) in
            HUD.hide()
        }

    }
    
}

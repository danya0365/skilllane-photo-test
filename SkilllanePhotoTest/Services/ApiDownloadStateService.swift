//
//  ApiDownloadStateService.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 12/12/2563 BE.
//

import RealmSwift

enum ApiDownloadStateName: String {
  case infinitePhoto
  case other
}

class ApiDownloadStateService {
    
    //MARK: - Properties
    static var shareInstance = ApiDownloadStateService()
    let realm = try! Realm()
    
    //MARK: - Functions
    func getApiDownloadState(_ apiDownloadStateName: ApiDownloadStateName, _ page: Int, _ limit: Int) -> ApiDownloadState {
        
        if let apiDownloadState = realm.objects(ApiDownloadState.self).filter(" name = '\(apiDownloadStateName.rawValue)' ").first {
            return apiDownloadState
        }
        let apiDownloadState = ApiDownloadState()
        apiDownloadState.name = apiDownloadStateName.rawValue
        apiDownloadState.page = page
        apiDownloadState.limit = limit
        try! realm.write {
            realm.add(apiDownloadState, update: .modified)
        }
        return apiDownloadState
    }
}

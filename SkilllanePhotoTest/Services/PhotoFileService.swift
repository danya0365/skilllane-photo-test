//
//  PhotoFileService.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 9/12/2563 BE.
//

import Foundation
import UIKit


class PhotoFileService {
    
    //MARK: - Properties
    static var shareInstance = PhotoFileService()
    
    //MARK: - Functions
    func saveImage(_ imageName: String, image: UIImage) {
        let fileManager = FileManager.default
        let imagePath = getImagePath(imageName)
        let data = image.pngData()
        
        DispatchQueue.global(qos: .background).async {
            fileManager.createFile(atPath: imagePath, contents: data, attributes: nil)
        }
    }
    
    func isImageDownloaded(_ imageName: String) -> Bool {
        let fileManager = FileManager.default
        let imagePath = getImagePath(imageName)
        if fileManager.fileExists(atPath: imagePath) {
            return true
        }
        return false
    }
    
    func getImagePath(_ imageName: String) -> String {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let imagePath = (NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        return imagePath
    }
}

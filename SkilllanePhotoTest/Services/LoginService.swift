//
//  LoginService.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 8/12/2563 BE.
//

import RealmSwift
import SwiftyJSON
import Alamofire

class LoginService {
    
    //MARK: - Properties
    static var shareInstance = LoginService()
    let realm = try! Realm()
    
    //MARK: - Functions
    func signIn(_ onSuccess: @escaping () -> (), onFailure: @escaping (_ error: String?) -> () ) {
        
        let token = Token()
        token.token = "test"
        
        try! realm.write {
            realm.add(token)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            onSuccess()
        }

    }
    
    func signOut(_ onSuccess: @escaping () -> (), onFailure: @escaping (_ error: String?) -> () ) {

        if let token = realm.objects(Token.self).first {
            realm.beginWrite()
            realm.delete(token)
            try! realm.commitWrite()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            onSuccess()
        }

    }
}

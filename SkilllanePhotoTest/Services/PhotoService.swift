//
//  PhotoService.swift
//  SkilllanePhotoTest
//
//  Created by Marosdee Uma on 8/12/2563 BE.
//

import RealmSwift
import SwiftyJSON
import Alamofire

class PhotoService {
    
    //MARK: - Properties
    static var shareInstance = PhotoService()
    let realm = try! Realm()
    var page = 1
    var limit = 3
    var apiDownloadState: ApiDownloadState!
    
    //MARK: - Functions
    init() {
        initApiDownloadState()
    }
    
    func initApiDownloadState() {
        apiDownloadState = ApiDownloadStateService.shareInstance.getApiDownloadState(.infinitePhoto, page, limit)
        page = apiDownloadState.page
        limit = apiDownloadState.limit
    }
    
    func insertDatabase(_ jsons: [JSON]?) {

        realm.beginWrite()
        var index = 0
        for json in jsons ?? [] {
            let photo = Photo(json)
            let orderNumber = Int("\(limit)\(String(format: "%05d", page))\(String(format: "%03d", index))") ?? 0
            photo.orderNumber = orderNumber
            photo.createdAt = Date()
            let oldPhoto = realm.objects(Photo.self).filter(" id = '\(photo.id)' ").first
            if (oldPhoto == nil) {
                realm.add(photo, update: .modified)
            }
            index += 1
        }
        
        if jsons?.count ?? 0 > 0  {
            self.apiDownloadState.page = self.page
        }
        try! realm.commitWrite()
    }
    
    
    func requestDataFromApi(_ onSuccess: @escaping () -> (), _ onFailure: @escaping (_ error: String?) -> () ) {
        
        let parameters: [String: Any] = [
            "page": page,
            "limit": limit
        ]
        
        let urlString = "https://picsum.photos/v2/list"
        
        AF.request(urlString, parameters: parameters)
            .responseJSON { response in

                switch (response.result) {

                     case .success( _):
                        
                        let json = response.value
                        if let jsonData = json as? [[String:Any]] {
                            let json = JSON(jsonData)
                            self.insertDatabase(json.array)
                            onSuccess()
                        }
                        break
                     case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                        onFailure(error.localizedDescription)
                        break
                 }
        }
    }
}

